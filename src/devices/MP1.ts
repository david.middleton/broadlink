import { BaseDevice } from './BaseDevice';

export interface IMP1State {
  socketId: 's1' | 's2' | 's3' | 's4';
  status: 'on' | 'off';
}

export class MP1 extends BaseDevice {
  type = 'MP1';

  constructor (host, mac, timeout = 10) {
    super(host, mac,timeout);

    this.on('payload', (err, data) => {
      if (err) { this.emit('error', err); }
      const state = {
        mac: this.mac,
        host: this.host,
        status: {
          s1: Boolean(data[0x0e] & 0x01) ? 'on' : 'off',
          s2: Boolean(data[0x0e] & 0x02) ? 'on' : 'off',
          s3: Boolean(data[0x0e] & 0x04) ? 'on' : 'off',
          s4: Boolean(data[0x0e] & 0x08) ? 'on' : 'off',
        },
      };
      this.emit('state', state);
    });
  }

  /**
   * Sets the power state of the smart power strip.
   * @param sid_mask
   * @param state
   */

  setPowerMask (sidMask, state) {
    const packet = Buffer.alloc(16,0);
    packet[0x00] = 0x0d;
    packet[0x02] = 0xa5;
    packet[0x03] = 0xa5;
    packet[0x04] = 0x5a;
    packet[0x05] = 0x5a;
    packet[0x06] = 0xb2 + (state ? (sidMask << 1) : sidMask);
    packet[0x07] = 0xc0;
    packet[0x08] = 0x02;
    packet[0x0a] = 0x03;
    packet[0x0d] = sidMask;
    packet[0x0e] = state ? sidMask : 0;
    this.sendPacket(0x6a, packet);
  }

  /**
   * Sets the power state of the smart power strip.
   */
  setPower (sid, state) {
    const sidMask = 0x01 << (sid - 1);
    // console.log(0xb2 + (state ? (sidMask << 1) : sidMask));
    this.setPowerMask(sidMask, state);
  }

  setState (state: IMP1State) {
    const [,sid] = state.socketId.match(/^s(\d)$/);
    this.setPower(sid, state.status === 'on');
  }

  /**
   * Returns the power state of the smart power strip in raw format
   */

  requestState () {
    const packet = Buffer.alloc(16,0);
    packet[0x00] = 0x0a;
    packet[0x02] = 0xa5;
    packet[0x03] = 0xa5;
    packet[0x04] = 0x5a;
    packet[0x05] = 0x5a;
    packet[0x06] = 0xae;
    packet[0x07] = 0xc0;
    packet[0x08] = 0x01;

    this.sendPacket(0x6a, packet);
  }
}
